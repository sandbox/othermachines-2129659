Migrate Domain Prefix to Domain Path

** NOT YET RECOMMENDED FOR PRODUCTION SITES. TESTERS NEEDED! **

This module assists in the migration from using Domain Prefix 6.x
(part of the Domain Access package [1]) to generate unique path aliases
for your affiliate domains, to Domain Path 7.x [2].

STEPS:

1. Back up your database!

2. Upgrade Drupal core to 7.x (see core file UPGRADE.txt)

3. Go through steps of upgrading Domain Access module but DO NOT enable
   any submodules until this process is finished.

   See: Upgrading to Drupal 7 using Domain Access [3]

4. Enable this module and all dependencies.

5. Go to Administration > Structure > Migrate Domain Prefix to Domain Path.

6. Select some or all of the domains. Optionally select the checkbox "Also
   remove duplicate aliases from original url_alias table." This will only
   apply to entities not assigned to the primary domain.

7. Click "Migrate".

8. Check your data in {domain_path} table, or via the UI.

9. If you are not using it for anything else, uninstall the Domain Prefix
   module. NOTE: THIS WILL REMOVE YOUR PREFIXED TABLES COMPLETELY.

10.Disable, uninstall and remove this module.


Created by othermachines [4].

[1] https://drupal.org/project/domain
[2] https://drupal.org/project/domain_path
[3] https://drupal.org/node/1074092
[4] https://drupal.org/user/1201100