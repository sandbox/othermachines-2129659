<?php

/**
 * @file
 */

/**
 * Submission form that triggers the migration.
 */
function migrate_domain_prefix_url_alias_form($form, &$form_state) {

  $form['migrate_domain_prefix_url_alias'] = array(
    '#type' => 'fieldset',
    '#title' => t('Migrate data from prefixed <em>url_alias</em> tables'),
    '#description' => t('Select the domains to migrate. Only one alias per entity, per language, will be migrated. Aliases belonging to non-existent entities will not be migrated.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $domains = domain_id_list();
  foreach ($domains as $k => $domain_id) {
    // Exclude default domain.
    if ($domain_id > 1) {
      $form['migrate_domain_prefix_url_alias'][$domain_id] = array(
        '#type' => 'checkbox',
        '#title' => $k,
        '#default_value' => 0,
      );
    }
  }

  $form['clean_url_alias'] = array(
    '#type' => 'checkbox',
    '#title' => t('Also remove duplicate aliases from original <em>url_alias</em> table.'),
    '#description' => t('This will only apply to entities not assigned to the primary domain.'),
    '#default_value' => 0,
  );

  $form['actions']['migrate'] = array(
    '#type' => 'submit',
    '#value' => t('Migrate'),
  );
  return $form;
}

/**
 * Submit handler for migrate_domain_prefix_url_alias_form().
 */
function migrate_domain_prefix_url_alias_form_submit(&$form, &$form_state) {
  $process_domains = array();
  $domains = domain_id_list();
  foreach ($domains as $k => $domain_id) {
    if (isset($form_state['values'][$domain_id]) && $form_state['values'][$domain_id]) {
      // Process this domain.
      $process_domains[] = $domain_id;
    }
  }

  $clean_url_alias = $form_state['values']['clean_url_alias'];

  migrate_domain_prefix_url_alias_init_batch($process_domains, $clean_url_alias);
}

/**
 * Initialize batch.
 */
function migrate_domain_prefix_url_alias_init_batch($domains, $clean_url_alias) {

  // Get some entity info which will help us parse our path strings.
  $entity_types = array();
  foreach (field_info_bundles() as $type => $v) {
    $info = entity_get_info($type);
    if (isset($info['path'])) {
      $entity_types[$type] = $info['path'];
    }
  }

  $operations = array();
  foreach ($domains as $domain_id) {
    $operations[] = array('migrate_domain_prefix_url_alias_batch', array($domain_id, $entity_types, $clean_url_alias));
  }

  $batch = array(
    'operations' => $operations,
    'title' => t('Migrating data'),
    'finished' => 'migrate_domain_prefix_url_alias_batch_finished',
    'init_message' => t('Migration is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Sorry, we have encountered an error.'),
    'file' => drupal_get_path('module', 'migrate_domain_prefix_url_alias') . '/migrate_domain_prefix_url_alias.admin.inc',
  );
  batch_set($batch);
  batch_process('admin/structure/migrate_domain_prefix_url_alias');
}

/**
 * Batch processing function.
 */
function migrate_domain_prefix_url_alias_batch($domain_id, $entity_types, $clean_url_alias, &$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox'] = array();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_pid'] = 0;
    $context['sandbox']['domain_id'] = $domain_id;
    $context['sandbox']['max'] = db_query('SELECT COUNT(DISTINCT pid) FROM {domain_' . $context['sandbox']['domain_id'] . '_url_alias}')->fetchField();
  }

  $limit = 25;

  // Retrieve aliases from all domains (excluding primary domain).
  $result = migrate_domain_prefix_url_alias_get_aliases($domain_id, $context['sandbox']['current_pid'], $limit);

  // Loops through batch of aliases.
  foreach ($result as $alias) {

    // Get entity type and id based on alias system path, then validate
    // that the entity exists.
    foreach ($entity_types as $type => $pattern) {
      if (strpos($alias->src, str_replace('_', '/', $type), 0) !== FALSE) {
        $entity_id = substr($alias->src, strpos($pattern, '%' . $type), strlen('%' . $type));
        $entity = entity_load($type, array($entity_id));
        if (!empty($entity)) {
          $entity_type = $type;
        }
        break;
      }
    }

    if (isset($entity_type)) {

      $grant_primary = isset($entity->domains[1]);

      // Attempt to retrieve the path.
      $conditions = array(
        'source' => $alias->src,
        'language' => $alias->language,
        'domain_id' => $domain_id,
      );
      $path = domain_path_path_load($conditions);
      if (!$path) {
        $path = array();
      }

      // Set the path alias.
      $path['alias'] = $alias->dst;

      // Fill in missing pieces.
      $path += array(
        'dpid' => NULL,
        'domain_id' => $domain_id,
        'source' => $alias->src,
        'language' => $alias->language ? $alias->language : LANGUAGE_NONE,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
      );

      domain_path_path_save($path, TRUE);

      // (Optionally) remove all aliases related to this entity from original
      // url_alias table unless the entity is also granted to primary domain.
      if ($clean_url_alias && !$grant_primary) {
        db_delete('url_alias')
          ->condition('source', $alias->src)
          ->condition('language', $alias->language)
          ->execute();
      }
    }

    $context['results'][] = check_plain($alias->src);
    $context['sandbox']['progress']++;
    $context['sandbox']['current_pid'] = $alias->pid;
    $context['message'] = t('Now processing %path for domain %domain', array('%path' => $alias->src, '%domain' => $domain_id));
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Helper function. Retrieve aliases from all [PREFIX_]url_alias tables.
 */
function migrate_domain_prefix_url_alias_get_aliases($domain_id, $pid, $limit) {
  if (!db_table_exists('domain_' . $domain_id . '_url_alias')) {
    return array();
  }
  return db_select('domain_' . $domain_id . '_url_alias')
    ->fields('domain_' . $domain_id . '_url_alias', array('pid', 'src', 'dst'))
    ->condition('pid', $pid, '>')
    ->orderBy('pid')
    ->range(0, $limit)
    ->execute()->fetchAllAssoc('pid');
}

/**
 * Batch 'finished' callback.
 */
function migrate_domain_prefix_url_alias_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('%results aliases processed.', array('%results' => count($results))));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE))), 'error');
  }
}

